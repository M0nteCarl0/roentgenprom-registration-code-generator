#include "GeneratorIndentNumber.h"

GeneratorIndentNumber::GeneratorIndentNumber(void) {}

GeneratorIndentNumber::~GeneratorIndentNumber(void) {}

string GeneratorIndentNumber::GetIndentificationNumber(void) {
  string Result;
  unsigned int _EAX;
  char data[2000];
  char SystemName[1000];
  char VolumeName[1000];
  char IndntNumberBuffer[1000];
  DWORD ProcessorFeaturesEAX;
  DWORD Flags;
  DWORD FileNameLength;
  DWORD type;
  DWORD len;
  DWORD SerialNumber;
  HKEY__ *hKeyVideo;
  CPUInformation Inform;
  Result.clear();
  GetVolumeInformationA("c://", VolumeName, 1000, &SerialNumber,
                        &FileNameLength, &Flags, SystemName, 1000);
  Inform.GetFeatureInformation();
  ProcessorFeaturesEAX = Inform._EAX;
  data[0] = 0;
  hKeyVideo = 0;
  RegOpenKeyExA(HKEY_LOCAL_MACHINE, "HARDWARE\\DESCRIPTION\\System", 0,
                0x20019u, &hKeyVideo);
  type = 7;
  len = 2000;

  RegQueryValueExA(hKeyVideo, "VideoBiosVersion", 0, &type, (LPBYTE)data, &len);
  RegCloseKey(hKeyVideo);
  int HDDSerialIndex = GetIndentification(data);
  int HighID = SerialNumber & 0x70F0F0F0 | ProcessorFeaturesEAX & 0xF0F |
               ((unsigned __int16)(ProcessorFeaturesEAX & 0xF0F0) << 12);
  int LowID = SerialNumber & 0xF0F0F0F | HDDSerialIndex & 0xF0F0 |
              ((HDDSerialIndex & 0x70F00) << 12);
  sprintf(IndntNumberBuffer, "%.10d%.10d", HighID, LowID);
  Result = string(IndntNumberBuffer);
  size_t Position = 2;

  do {
    Result.insert(Position, "-");
    Position += 3;
  } while (Position < 29);

  return Result;
}

DWORD GeneratorIndentNumber::GetSerialNumberHDD(int Id, string &Result) {

  DWORD dwRet = NO_ERROR;
  string SerialNumberHDD;

  // Format physical drive path (may be '\\.\PhysicalDrive0',
  // '\\.\PhysicalDrive1' and so on).
  char strDrivePath[255];
  sprintf(strDrivePath, "\\\\.\\PhysicalDrive%u", Id);
  // Get a handle to physical drive
  HANDLE hDevice =
      ::CreateFile(strDrivePath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                   OPEN_EXISTING, 0, NULL);

  if (INVALID_HANDLE_VALUE == hDevice)
    return ::GetLastError();

  // Set the input data structure
  STORAGE_PROPERTY_QUERY storagePropertyQuery;
  ZeroMemory(&storagePropertyQuery, sizeof(STORAGE_PROPERTY_QUERY));
  storagePropertyQuery.PropertyId = StorageDeviceProperty;
  storagePropertyQuery.QueryType = PropertyStandardQuery;

  // Get the necessary output buffer size
  STORAGE_DESCRIPTOR_HEADER storageDescriptorHeader = {0};
  DWORD dwBytesReturned = 0;
  if (!::DeviceIoControl(
          hDevice, IOCTL_STORAGE_QUERY_PROPERTY, &storagePropertyQuery,
          sizeof(STORAGE_PROPERTY_QUERY), &storageDescriptorHeader,
          sizeof(STORAGE_DESCRIPTOR_HEADER), &dwBytesReturned, NULL)) {
    dwRet = ::GetLastError();
    ::CloseHandle(hDevice);
    return dwRet;
  }

  // Alloc the output buffer
  const DWORD dwOutBufferSize = storageDescriptorHeader.Size;
  BYTE *pOutBuffer = new BYTE[dwOutBufferSize];
  ZeroMemory(pOutBuffer, dwOutBufferSize);

  // Get the storage device descriptor
  if (!::DeviceIoControl(hDevice, IOCTL_STORAGE_QUERY_PROPERTY,
                         &storagePropertyQuery, sizeof(STORAGE_PROPERTY_QUERY),
                         pOutBuffer, dwOutBufferSize, &dwBytesReturned, NULL)) {
    dwRet = ::GetLastError();
    delete[] pOutBuffer;
    ::CloseHandle(hDevice);
    return dwRet;
  }

  // Now, the output buffer points to a STORAGE_DEVICE_DESCRIPTOR structure
  // followed by additional info like vendor ID, product ID, serial number, and
  // so on.
  STORAGE_DEVICE_DESCRIPTOR *pDeviceDescriptor =
      (STORAGE_DEVICE_DESCRIPTOR *)pOutBuffer;
  const DWORD dwSerialNumberOffset = pDeviceDescriptor->SerialNumberOffset;
  if (dwSerialNumberOffset != 0) {
    // Finally, get the serial number
    SerialNumberHDD = string((const char *)pOutBuffer + dwSerialNumberOffset);
    Result = string(SerialNumberHDD);
  }

  // Do cleanup and return
  delete[] pOutBuffer;
  ::CloseHandle(hDevice);
  return dwRet;
}

int GeneratorIndentNumber::GetIndentification(char *DataBufferVideo) {
  int Result = 0;
  string HDDSerial;
  GetSerialNumberHDD(0, HDDSerial);
  const char Key[] = "����WD-W";
  int NumSmbs = strncmp(HDDSerial.c_str(), &Key[4], 4);
  int v1 = 0;
  char *v3 = DataBufferVideo;
  if (!NumSmbs)
    v3 = DataBufferVideo + 5;
  for (; v3; ++v3) {
    if (!*v3)
      break;
    switch (*v3) {
    case 54:
    case 71:
    case 81:
    case 103:
    case 113:
      v1 += 6;
      goto EXITLABEL;
    case 55:
    case 72:
    case 82:
    case 104:
    case 114:
      v1 += 7;
      goto EXITLABEL;
    case 56:
    case 73:
    case 83:
    case 105:
    case 115:
      v1 += 8;
      goto EXITLABEL;
    case 57:
    case 74:
    case 84:
    case 106:
    case 116:
      v1 += 9;
      goto EXITLABEL;
    case 49:
    case 66:
    case 76:
    case 86:
    case 98:
    case 108:
    case 118:
      ++v1;
      goto EXITLABEL;
    case 50:
    case 67:
    case 77:
    case 87:
    case 99:
    case 109:
    case 119:
      v1 += 2;
      goto EXITLABEL;
    case 51:
    case 68:
    case 78:
    case 88:
    case 100:
    case 110:
    case 120:
      v1 += 3;
      goto EXITLABEL;
    case 52:
    case 69:
    case 79:
    case 89:
    case 101:
    case 111:
    case 121:
      v1 += 4;
      goto EXITLABEL;
    case 53:
    case 70:
    case 80:
    case 90:
    case 102:
    case 112:
    case 122:
      v1 += 5;
      goto EXITLABEL;
    case 48:
    case 65:
    case 75:
    case 85:
    case 97:
    case 107:
    case 117:
    EXITLABEL:
      v1 *= 7;
      break;
    default:
      continue;
    }
  }
  return v1;
}
