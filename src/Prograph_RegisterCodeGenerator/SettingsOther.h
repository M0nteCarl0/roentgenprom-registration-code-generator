#pragma once
#include "settingstemplate.h"
class SettingsOther :
    public SettingsTemplate
    {
    public:
        SettingsOther(void);
        ~SettingsOther(void);
        char m_bSettingsRead;
        char m_bForce;
        unsigned int m_nStateNav;
        unsigned int m_nStateTools;
        unsigned int m_nStateShot;
        unsigned int m_nDbX;
        unsigned int m_nDbY;
        unsigned int m_nMainX;
        unsigned int m_nMainY;
        string m_strRegistrationKey;
        string m_strHKCUDate;
        string m_strHKLMDate;
        unsigned int m_nDBCompressCounter;
        char *m_pPatColWidths;
        unsigned int m_nPatColWidthsLen;
        char *m_pPhotoRect;
        unsigned int m_nPhotoRectLen;
        int m_bHKLMTS;
        int ReadSettingsHKCU(SettingsOther* SettingsOther);
    };

