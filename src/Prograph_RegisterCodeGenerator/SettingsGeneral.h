#pragma once
#include "settingstemplate.h"
class SettingsGeneral :
    public SettingsTemplate
    {
    public:
        SettingsGeneral(void);
        ~SettingsGeneral(void);
        string m_strOrgNam;
        unsigned int m_nArmType;
        unsigned int m_nDobFormat;
        int m_bAutoStart;
        int m_bSelfShutDown;
        string m_strDevName;
        int m_bVkbd;
        string m_strDevSerNum;
        string m_strArmNum;
        string m_strWorkbench;
        int m_bIsMobile;
        int m_bHybrid;
        string m_strSite;
        int m_nLogLevel;
        int m_bAskSetPasswd;
        int m_bIdentityOn;
        int m_bShowPatPhoto;
        int m_bUsePrefixPhotoPath;
        string m_strPrefixPhotoPath;
        int m_bAutoLoadDefaults;
        int m_bContainerDf3d;
        string m_strTsPult;
        string m_strTsReconstruct;
        unsigned int m_nAmplType;
    };

