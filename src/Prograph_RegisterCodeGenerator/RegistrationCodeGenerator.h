#pragma once
#include <string>
#include "MSHA1.h"
using namespace std;
class RegistrationCodeGenerator {
public:
  RegistrationCodeGenerator(void);
  ~RegistrationCodeGenerator(void);
  static string GenerateRegistrationCode(string &IndentficationCode);
};
