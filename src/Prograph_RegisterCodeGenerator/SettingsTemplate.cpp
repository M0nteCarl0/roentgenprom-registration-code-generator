

#include "SettingsTemplate.h"
SettingsTemplate::SettingsTemplate(void) {}

SettingsTemplate::~SettingsTemplate(void) {}

int SettingsTemplate::OwnWriteProfileIntHKCU(string key, int val) {

  int Result;
  LSTATUS RegOpStatus;
  return 0;
}

int SettingsTemplate::OwnWriteProfileStringHKCU(string key, string val) {
  int Result;
  LSTATUS RegOpStatus;
  return 0;
}

int SettingsTemplate::OwnWriteProfileBinaryHKCU(string key, char *val,
                                                DWORD len) {
  int Result;
  LSTATUS RegOpStatus;
  return 0;
}

int SettingsTemplate::OwnGetProfileIntHKCU(string key, int *val) {
  int Result = 0;
  DWORD Len = REG_DWORD;
  DWORD Type = REG_DWORD;
  HKEY RegOpResultHKCU;
  HKEY RegOpResultHKLM;
  string RegBasePath = ProgramSettings::GetRegistryPath(RegBasePath);
  LSTATUS RegOpStatus = RegOpenKeyEx(HKEY_CURRENT_USER, RegBasePath.c_str(), 0,
                                     KEY_ALL_ACCESS, &RegOpResultHKCU);
  if (RegOpStatus == ERROR_SUCCESS) {
    RegOpStatus = RegQueryValueEx(RegOpResultHKCU, key.c_str(), NULL, &Type,
                                  (LPBYTE)&val, &Len);
    if (RegOpStatus == ERROR_SUCCESS) {
      RegOpStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, RegBasePath.c_str(), 0,
                                 KEY_READ, &RegOpResultHKLM);
      if (RegOpStatus == ERROR_SUCCESS) {
        RegOpStatus = RegQueryValueEx(RegOpResultHKLM, key.c_str(), NULL, &Type,
                                      (LPBYTE)&val, &Len);

        if (RegOpStatus == ERROR_SUCCESS) {
          RegOpStatus = RegSetValueExA(RegOpResultHKCU, key.c_str(), 0, Type,
                                       (const BYTE *)&val, Len);

          RegCloseKey(RegOpResultHKCU);
        } else {
          RegCloseKey(RegOpResultHKCU);
          RegCloseKey(RegOpResultHKLM);
        }
      }
    }

  } else {
    RegCloseKey(RegOpResultHKCU);
    Result = RegOpStatus;
  }
  return Result;
}

int SettingsTemplate::OwnGetProfileStringHKCU(string key, string val) {
  int Result = 0;
  DWORD Len = 2048;
  DWORD Type = REG_SZ;
  HKEY RegOpResultHKCU;
  HKEY RegOpResultHKLM;
  string RegBasePath = ProgramSettings::GetRegistryPath(RegBasePath);
  LSTATUS RegOpStatus = RegOpenKeyEx(HKEY_CURRENT_USER, RegBasePath.c_str(), 0,
                                     KEY_ALL_ACCESS, &RegOpResultHKCU);

  val.resize(Len);
  if (RegOpStatus == ERROR_SUCCESS) {
    RegOpStatus = RegQueryValueEx(RegOpResultHKCU, key.c_str(), NULL, &Type,
                                  (LPBYTE)val.c_str(), &Len);
    if (RegOpStatus == ERROR_SUCCESS) {
      RegOpStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, RegBasePath.c_str(), 0,
                                 KEY_READ, &RegOpResultHKLM);
      if (RegOpStatus == ERROR_SUCCESS) {
        RegOpStatus = RegQueryValueEx(RegOpResultHKLM, key.c_str(), NULL, &Type,
                                      (LPBYTE)val.c_str(), &Len);

        if (RegOpStatus == ERROR_SUCCESS) {
          int LenLocal = CheckNullTerminateAndSet(Len, 2048, &val);
          RegOpStatus = RegSetValueExA(RegOpResultHKCU, key.c_str(), 0, Type,
                                       (const BYTE *)val.c_str(), LenLocal);
          RegCloseKey(RegOpResultHKCU);

        } else {
          RegCloseKey(RegOpResultHKCU);
          RegCloseKey(RegOpResultHKLM);
        }
      }
    }

  } else {
    RegCloseKey(RegOpResultHKCU);
    Result = RegOpStatus;
  }

  return Result ;
}

int SettingsTemplate::OwnGetProfileBinaryHKCU(string key, char **val,
                                              DWORD *len) {
  int Result = 0;
  HKEY RegOpResult;
  DWORD Type = REG_BINARY;
  HKEY RegOpResultHKCU;
  HKEY RegOpResultHKLM;
  string RegBasePath = ProgramSettings::GetRegistryPath(RegBasePath);
  LSTATUS RegOpStatus = RegOpenKeyEx(HKEY_CURRENT_USER, RegBasePath.c_str(), 0,
                                     KEY_ALL_ACCESS, &RegOpResultHKCU);
  if (RegOpStatus == ERROR_SUCCESS) {
    RegOpStatus = RegQueryValueEx(RegOpResultHKCU, key.c_str(), NULL, &Type,
                                  (LPBYTE)&val, (DWORD *)len);
    if (RegOpStatus == ERROR_SUCCESS) {
      RegOpStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, RegBasePath.c_str(), 0,
                                 KEY_READ, &RegOpResultHKLM);
      if (RegOpStatus == ERROR_SUCCESS) {
        RegOpStatus = RegQueryValueEx(RegOpResultHKLM, key.c_str(), NULL, &Type,
                                      (LPBYTE)&val, (DWORD *)len);
        if (RegOpStatus == ERROR_SUCCESS) {
          RegOpStatus = RegSetValueExA(RegOpResultHKCU, key.c_str(), 0, Type,
                                       (const BYTE *)&val, *len);

          RegCloseKey(RegOpResultHKCU);

        } else {
          RegCloseKey(RegOpResultHKCU);
          RegCloseKey(RegOpResultHKLM);
        }
      }
    }

  } else {
    RegCloseKey(RegOpResultHKCU);
    Result = RegOpStatus;
  }
  return Result;
}

int SettingsTemplate::OwnWriteProfileIntHKLM(string key, int val) {
  int Result;
  LSTATUS RegOpStatus;
  return 0;
}

int SettingsTemplate::OwnWriteProfileStringHKLM(string key, string val) {
  int Result;
  LSTATUS RegOpStatus;
  return 0;
}

int SettingsTemplate::OwnGetProfileBinaryHKLM(string key, char **val,
                                              DWORD *len) {
  int Result = 0;
  HKEY RegOpResult;
  string RegBasePath = ProgramSettings::GetRegistryPath(RegBasePath);
  LSTATUS RegOpStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, RegBasePath.c_str(), 0,
                                     KEY_ALL_ACCESS, &RegOpResult);
  if (RegOpStatus = ERROR_SUCCESS) {

  } else {
    Result = RegOpStatus;
  }
  return Result;
}

int SettingsTemplate::OwnGetProfileStringHKLM(string key, string val) {

  int Result = 0;
  HKEY RegOpResult;
  string RegBasePath = ProgramSettings::GetRegistryPath(RegBasePath);
  LSTATUS RegOpStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, RegBasePath.c_str(), 0,
                                     KEY_ALL_ACCESS, &RegOpResult);
  if (RegOpStatus = ERROR_SUCCESS) {

  } else {
    Result = RegOpStatus;
  }
  return 0;
}

int SettingsTemplate::OwnGetProfileIntHKLM(string key, int *val) { return 0; }

int SettingsTemplate::CheckNullTerminateAndSet(unsigned int len,
                                               const int nMaxRegSZ,
                                               string *value) {
  unsigned int Length;
  char *Buffer = (char *)value->data();
  Length = len;
  if (value->data()[len - 1]) {
    if (len + 1 >= nMaxRegSZ) {
      value->resize(len + 1);
    }
    Buffer[len] = 0;
    *value = Buffer;
    Length = len + 1;
  }

  return Length;
}
