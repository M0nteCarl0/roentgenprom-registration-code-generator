#pragma once
#include "GeneratorIndentNumber.h"
#include "RegistrationCodeGenerator.h"
#include "CryptoNite.h"
#include "sha1-master\sha1.h"
namespace Prograph_RegisterCodeGenerator {
using namespace System::Runtime::InteropServices;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

/// <summary>
/// ������ ��� Form1
/// </summary>
public
ref class GeneratorView : public System::Windows::Forms::Form {
public:
  GeneratorView(void) {
    InitializeComponent();
	string IndentNumber = GeneratorIndentNumber::GetIndentificationNumber();
    IndentificationCodetextBox->Text = gcnew System::String(
		IndentNumber.c_str());
	RegistartionCodetextBox->Text = gcnew String(RegistrationCodeGenerator::GenerateRegistrationCode(IndentNumber).c_str());
  }

protected:
  /// <summary>
  /// ���������� ��� ������������ �������.
  /// </summary>
  ~GeneratorView() {
    if (components) {
      delete components;
    }
  }
private: System::Windows::Forms::Button^  GenerateindetificationNumberbutton;
private: System::Windows::Forms::Button^  GenerateRegistrationCodebutton;
protected:

private:


protected:
private:


private:
  System::Windows::Forms::Label ^ label1;

private:
  System::Windows::Forms::Label ^ label2;

private:
  System::Windows::Forms::TextBox ^ IndentificationCodetextBox;

private:
  System::Windows::Forms::TextBox ^ RegistartionCodetextBox;
private: System::Windows::Forms::Button^  CopyRegistrationCodebutton;

private:


private:
private:
private:
  /// <summary>
  /// ��������� ���������� ������������.
  /// </summary>
  System::ComponentModel::Container ^ components;

#pragma region Windows Form Designer generated code
  /// <summary>
  /// ������������ ����� ��� ��������� ������������ - �� ���������
  /// ���������� ������� ������ ��� ������ ��������� ����.
  /// </summary>
  void InitializeComponent(void) {
      this->GenerateindetificationNumberbutton = (gcnew System::Windows::Forms::Button());
      this->GenerateRegistrationCodebutton = (gcnew System::Windows::Forms::Button());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->IndentificationCodetextBox = (gcnew System::Windows::Forms::TextBox());
      this->RegistartionCodetextBox = (gcnew System::Windows::Forms::TextBox());
      this->CopyRegistrationCodebutton = (gcnew System::Windows::Forms::Button());
      this->SuspendLayout();
      // 
      // GenerateindetificationNumberbutton
      // 
      this->GenerateindetificationNumberbutton->Location = System::Drawing::Point(16, 114);
      this->GenerateindetificationNumberbutton->Name = L"GenerateindetificationNumberbutton";
      this->GenerateindetificationNumberbutton->Size = System::Drawing::Size(188, 23);
      this->GenerateindetificationNumberbutton->TabIndex = 0;
      this->GenerateindetificationNumberbutton->Text = L"Generate Indetification Number";
      this->GenerateindetificationNumberbutton->UseVisualStyleBackColor = true;
      this->GenerateindetificationNumberbutton->Click += gcnew System::EventHandler(this, &GeneratorView::GenerateindetificationNumberbutton_Click);
      // 
      // GenerateRegistrationCodebutton
      // 
      this->GenerateRegistrationCodebutton->Location = System::Drawing::Point(238, 114);
      this->GenerateRegistrationCodebutton->Name = L"GenerateRegistrationCodebutton";
      this->GenerateRegistrationCodebutton->Size = System::Drawing::Size(188, 23);
      this->GenerateRegistrationCodebutton->TabIndex = 1;
      this->GenerateRegistrationCodebutton->Text = L"Generate Registration  Code";
      this->GenerateRegistrationCodebutton->UseVisualStyleBackColor = true;
      this->GenerateRegistrationCodebutton->Click += gcnew System::EventHandler(this, &GeneratorView::GenerateRegistrationCodebutton_Click);
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(13, 15);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(113, 13);
      this->label1->TabIndex = 2;
      this->label1->Text = L"Indentification Number";
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(13, 47);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(94, 13);
      this->label2->TabIndex = 3;
      this->label2->Text = L"Registration  Code";
      // 
      // IndentificationCodetextBox
      // 
      this->IndentificationCodetextBox->Location = System::Drawing::Point(137, 12);
      this->IndentificationCodetextBox->Name = L"IndentificationCodetextBox";
      this->IndentificationCodetextBox->Size = System::Drawing::Size(196, 20);
      this->IndentificationCodetextBox->TabIndex = 4;
      // 
      // RegistartionCodetextBox
      // 
      this->RegistartionCodetextBox->Location = System::Drawing::Point(137, 44);
      this->RegistartionCodetextBox->Name = L"RegistartionCodetextBox";
      this->RegistartionCodetextBox->Size = System::Drawing::Size(196, 20);
      this->RegistartionCodetextBox->TabIndex = 5;
      // 
      // CopyRegistrationCodebutton
      // 
      this->CopyRegistrationCodebutton->Location = System::Drawing::Point(16, 80);
      this->CopyRegistrationCodebutton->Name = L"CopyRegistrationCodebutton";
      this->CopyRegistrationCodebutton->Size = System::Drawing::Size(188, 23);
      this->CopyRegistrationCodebutton->TabIndex = 6;
      this->CopyRegistrationCodebutton->Text = L"Copy Registration Code";
      this->CopyRegistrationCodebutton->UseVisualStyleBackColor = true;
      this->CopyRegistrationCodebutton->Click += gcnew System::EventHandler(this, &GeneratorView::CopyRegistrationCodebutton_Click);
      // 
      // GeneratorView
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
      this->ClientSize = System::Drawing::Size(445, 142);
      this->Controls->Add(this->CopyRegistrationCodebutton);
      this->Controls->Add(this->RegistartionCodetextBox);
      this->Controls->Add(this->IndentificationCodetextBox);
      this->Controls->Add(this->label2);
      this->Controls->Add(this->label1);
      this->Controls->Add(this->GenerateRegistrationCodebutton);
      this->Controls->Add(this->GenerateindetificationNumberbutton);
      this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
      this->Name = L"GeneratorView";
      this->Text = L"Prograph Registration Code Generator by M0nteCarl0";
      this->ResumeLayout(false);
      this->PerformLayout();

      }
#pragma endregion
private: System::Void CopyRegistrationCodebutton_Click(System::Object^  sender, System::EventArgs^  e) {
               Clipboard::SetText(RegistartionCodetextBox->Text);
             }
private: System::Void GenerateindetificationNumberbutton_Click(System::Object^  sender, System::EventArgs^  e) {
	string IndentNumber = GeneratorIndentNumber::GetIndentificationNumber();
	IndentificationCodetextBox->Text = gcnew System::String(
	IndentNumber.c_str());
}
private: System::Void GenerateRegistrationCodebutton_Click(System::Object^  sender, System::EventArgs^  e) {
	if (IndentificationCodetextBox->Text != String::Empty)
	{
		string IndentNumber = string((char*)Marshal::StringToHGlobalAnsi(IndentificationCodetextBox->Text).ToPointer());
		RegistartionCodetextBox->Text = gcnew String(RegistrationCodeGenerator::GenerateRegistrationCode(IndentNumber).c_str());
	}
	else
	{
		MessageBox::Show(" Empty Indentification Number! Please enter Indetification Number or press button Generate Indentification Number and try again generate Registration Code !", " Empty Indetification Number !", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}
};
} // namespace Prograph_RegisterCodeGenerator
