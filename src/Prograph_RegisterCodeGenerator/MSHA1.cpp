#include "MSHA1.h"

#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

/* blk0() and blk() perform the initial expand. */
/* I got the idea of expanding during the round function from SSLeay */
#if BYTE_ORDER == LITTLE_ENDIAN
#define blk0(i) (block->l[i] = (rol(block->l[i],24)&0xFF00FF00) \
    |(rol(block->l[i],8)&0x00FF00FF))
#elif BYTE_ORDER == BIG_ENDIAN
#define blk0(i) block->l[i]
#else
#error "Endianness not defined!"
#endif
#define blk(i) (block->l[i&15] = rol(block->l[(i+13)&15]^block->l[(i+8)&15] \
    ^block->l[(i+2)&15]^block->l[i&15],1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk0(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R1(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R2(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0x6ED9EBA1+rol(v,5);w=rol(w,30);
#define R3(v,w,x,y,z,i) z+=(((w|x)&y)|(w&x))+blk(i)+0x8F1BBCDC+rol(v,5);w=rol(w,30);
#define R4(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0xCA62C1D6+rol(v,5);w=rol(w,30);


FILE* TraceWork;
MSHA1::MSHA1(void) {}
void MSHA1::MSHAInit(MSHA1 *Source) {
	Source->block =(SHA1_WORKSPACE_BLOCK*) Source->workspace;;
	TraceWork = fopen("TraceSHA1.log", "w");
  MSHA1::Reset(Source);
 
}

MSHA1::~MSHA1(void) {  }

void MSHA1::Reset(MSHA1 *Source) {

  Source->state[0] = 0x67452301;
  Source->state[1] = 0xEFCDAB89;
  Source->state[2] = 0x98BADCFE;
  Source->state[3] = 0x10325476;
  Source->state[4] = 0xC3D2E1F0;
  Source->count[0] = 0;
  Source->count[1] = 0;
  memset(Source->block, 0, 64);
  memset(Source->buffer, 0, 64);
  fprintf(TraceWork,"Init Done");
}

void MSHA1::Final(MSHA1 * Source)
{
	MSHA1 *v1; // esi@1
	unsigned int v2; // eax@1
	unsigned int v3; // edx@2
	unsigned int v4; // eax@5
	unsigned int SHA1_Digest_Message; // edx@6
	int v6; // eax@7
	char v7; // [sp+Bh] [bp-9h]@2
	char finalcount[8]; // [sp+Ch] [bp-8h]@1

	finalcount[0] = 0;
	finalcount[1] = 0;
	finalcount[2] = 0;
	finalcount[3] = 0;
	finalcount[4] = 0;
	finalcount[5] = 0;
	finalcount[6] = 0;
	finalcount[7] = 0;
	v2 = 0;
	do
	{
		v3 = Source->count[v2 < 4] >> (24 - 8 * (v2 & 3));
		//*(&v7 + ++v2) = v3;
		finalcount[++v2] = v3;
	} while (v2 < 8);
	fprintf(TraceWork, "Final procedure");
	for (size_t i = 0; i < 8; i++)
	{
		fprintf(TraceWork,"FinalCount[%i] = %x\n", i, finalcount[i]);
		fflush(TraceWork);
	}

	v4 = 0;
	do
	{
		SHA1_Digest_Message = Source->state[v4 >> 2] >> (24 - 8 * (v4 & 3));
		fprintf(TraceWork, "Original_Digest[%i] = %x\n", v4, SHA1_Digest_Message);
		fflush(TraceWork);
		v4++;
		//Source->buffer[++v4 + 63] = SHA1_Digest_Message;
		//Source->digest[v4] = SHA1_Digest_Message;
	} while (v4 < 20);

	fclose(TraceWork);
	{
		MSHA1::Update(Source, "A", 1u);
	}

	while ((Source->count[0] & 0x1F8) != 448)
	MSHA1::Update(Source, "A", 1u);
	MSHA1::Update(Source, finalcount, 8u);
	v4 = 0;
	do
	{
		SHA1_Digest_Message = Source->state[v4 >> 2] >> (24 - 8 * (v4 & 3));
		Source->buffer[++v4 + 63] = SHA1_Digest_Message;
		Source->digest[v4] = SHA1_Digest_Message;
	} while (v4 < 20);
	memset(Source->buffer, 0, sizeof(Source->buffer));
	Source->state[0] = 0;
	Source->state[1] = 0;
	Source->state[2] = 0;
	Source->state[3] = 0;
	Source->state[4] = 0;
	Source->count[0] = 0;
	Source->count[1] = 0;
	MSHA1::Transform(Source, Source->state, Source->buffer);
}

void MSHA1::ReportHash(MSHA1 *Source, char *szReport, char uReportType) {

	
  MSHA1 *v3;        // ebx@1
  char *v4;         // ebp@2
  int v5;           // eax@2
  signed int v6;    // ebx@2
  bool v7;          // zf@4
  signed int v8;    // eax@4
  signed int v9;    // ebp@12
  char *v10;        // ebx@12
  char szTemp[5];   // [sp+10h] [bp-8h]@2
  int uReportTypea; // [sp+20h] [bp+8h]@2

  v3 = Source;
  if (uReportType) {
    if (uReportType == 1) {
      sprintf(szTemp, "%u", Source->digest[0]);
      v9 = 19;
      strcpy(szReport, szTemp);
      v10 = &v3->digest[1];
      do {
        sprintf(szTemp, "%u", (unsigned __int8)*v10);
        strcat(szReport, szTemp);
        ++v10;
        --v9;
      } while (v9);
    } else {
       strcpy(szReport, "ErrorReportType");
    }
  } else {
    sprintf(szTemp, "%02X", Source->digest[0]);
    v4 = &v3->digest[1];
    strcpy(szReport, szTemp);
    v5 = -96 - (DWORD)v3;
    uReportTypea = -96 - (DWORD)v3;
    v6 = 19;
    while (1) {
      v8 = (unsigned int)&v4[v5] & 0x80000001;
      v7 = v8 == 0;
      if (v8 < 0)
        v7 = (((BYTE)v8 - 1) | 0xFFFFFFFE) == -1;
      if (v7)
        sprintf(szTemp, "-%02X", (unsigned __int8)*v4);
      else
        sprintf(szTemp, "%02X", (unsigned __int8)*v4);
      strcat(szReport, szTemp);
      ++v4;
      if (!--v6)
        break;
      v5 = uReportTypea;
    }
  }
  
}

void MSHA1::Update(MSHA1 *Source, char *data, unsigned int len) {

  unsigned int v4; // ecx@1
  unsigned int v5; // edx@1
  int v6;          // eax@1
  int v7;          // ebp@4
  unsigned int i;  // esi@4

  // v3 = this;
  v4 = len;
  v5 = Source->count[0] + 8 * len;
  v6 = (Source->count[0] >> 3) & 0x3F;
  Source->count[0] = v5;
  if (v5 < 8 * len)
    ++Source->count[1];
  Source->count[1] += len >> 29;
  if (v6 + len <= 63) {
    v7 = 0;
  } else {
    v7 = 64 - v6;
    memcpy(&Source->buffer[v6], data, 64 - v6);
    MSHA1::Transform(Source, Source->state, Source->buffer);
    for (i = v7 + 63; i < len; v7 += 64) {
      MSHA1::Transform(Source, Source->state, &data[i - 63]);
      i += 64;
    }
    v4 = len;
    v6 = 0;
  }
  memcpy(&Source->buffer[v6], &data[v7], v4 - v7);
}

void MSHA1::Transform(MSHA1 *Source, unsigned int *state, char *buffer) {


  MSHA1 *v3;                  // eax@1
  unsigned int *v4;           // ecx@1
  unsigned int v5;            // edx@1
  unsigned int c;             // ST10_4@1
  unsigned int d;             // ST14_4@1
  unsigned int e;             // ST1C_4@1
  unsigned int v9;            // ebx@1
  SHA1_WORKSPACE_BLOCK *v10;  // ebp@1
  unsigned int v11;           // esi@1
  unsigned int v12;           // edi@1
  int a;                      // ST18_4@1
  unsigned int v14;           // edx@1
  int v15;                    // ST1C_4@1
  unsigned int v16;           // esi@1
  int v17;                    // ST14_4@1
  unsigned int v18;           // edi@1
  int v19;                    // ST10_4@1
  unsigned int v20;           // edx@1
  unsigned int v21;           // esi@1
  int v22;                    // ST18_4@1
  unsigned int v23;           // edi@1
  int v24;                    // ST1C_4@1
  unsigned int v25;           // edx@1
  int v26;                    // ST14_4@1
  unsigned int v27;           // esi@1
  int v28;                    // ST10_4@1
  unsigned int v29;           // edi@1
  unsigned int v30;           // edx@1
  int v31;                    // ST18_4@1
  unsigned int v32;           // esi@1
  int v33;                    // ST1C_4@1
  unsigned int v34;           // edi@1
  int v35;                    // ebx@1
  unsigned int v36;           // ST14_4@1
  unsigned int v37;           // ebx@1
  int v38;                    // ST10_4@1
  unsigned int v39;           // esi@1
  unsigned int v40;           // edi@1
  int v41;                    // ST18_4@1
  unsigned int v42;           // edx@1
  unsigned int v43;           // ebx@1
  int v44;                    // ST1C_4@1
  unsigned int v45;           // edx@1
  unsigned int v46;           // esi@1
  int v47;                    // ST14_4@1
  unsigned int v48;           // edx@1
  unsigned int v49;           // edi@1
  int v50;                    // ST10_4@1
  unsigned int v51;           // edx@1
  SHA1_WORKSPACE_BLOCK *v52;  // ebp@1
  unsigned int v53;           // ebx@1
  unsigned int v54;           // edx@1
  SHA1_WORKSPACE_BLOCK *v55;  // ebp@1
  unsigned int v56;           // esi@1
  int v57;                    // ST18_4@1
  unsigned int v58;           // edx@1
  unsigned int v59;           // edi@1
  int v60;                    // ebp@1
  unsigned int v61;           // edx@1
  unsigned int v62;           // ebx@1
  int v63;                    // ST14_4@1
  unsigned int v64;           // edx@1
  unsigned int v65;           // esi@1
  int v66;                    // ST10_4@1
  unsigned int v67;           // edx@1
  unsigned int v68;           // edi@1
  unsigned int v69;           // edx@1
  SHA1_WORKSPACE_BLOCK *v70;  // ebp@1
  unsigned int v71;           // ebx@1
  int v72;                    // ST18_4@1
  unsigned int v73;           // edx@1
  unsigned int v74;           // esi@1
  int v75;                    // ebp@1
  unsigned int v76;           // edx@1
  unsigned int v77;           // edi@1
  int v78;                    // ST14_4@1
  unsigned int v79;           // edx@1
  unsigned int v80;           // ebx@1
  int v81;                    // ST10_4@1
  unsigned int v82;           // edx@1
  unsigned int v83;           // esi@1
  unsigned int v84;           // edx@1
  SHA1_WORKSPACE_BLOCK *v85;  // ebp@1
  unsigned int v86;           // edi@1
  int v87;                    // ST18_4@1
  unsigned int v88;           // edx@1
  unsigned int v89;           // ebx@1
  int v90;                    // ebp@1
  unsigned int v91;           // edx@1
  unsigned int v92;           // esi@1
  int v93;                    // ST14_4@1
  unsigned int v94;           // edx@1
  unsigned int v95;           // edi@1
  int v96;                    // ST10_4@1
  unsigned int v97;           // edx@1
  unsigned int v98;           // ebx@1
  unsigned int v99;           // edx@1
  SHA1_WORKSPACE_BLOCK *v100; // ebp@1
  unsigned int v101;          // esi@1
  int v102;                   // ST18_4@1
  unsigned int v103;          // edx@1
  unsigned int v104;          // edi@1
  int v105;                   // ST1C_4@1
  unsigned int v106;          // edx@1
  unsigned int v107;          // edx@1
  int v108;                   // ST14_4@1
  unsigned int v109;          // esi@1
  unsigned int v110;          // esi@1
  int v111;                   // ST10_4@1
  unsigned int v112;          // edi@1
  unsigned int v113;          // edi@1
  int v114;                   // ebx@1
  unsigned int v115;          // edx@1
  unsigned int v116;          // edx@1
  int v117;                   // ST18_4@1
  unsigned int v118;          // esi@1
  unsigned int v119;          // esi@1
  int v120;                   // ST1C_4@1
  unsigned int v121;          // edi@1
  unsigned int v122;          // edi@1
  int v123;                   // ST14_4@1
  unsigned int v124;          // edx@1
  unsigned int v125;          // edx@1
  int v126;                   // ST10_4@1
  unsigned int v127;          // esi@1
  unsigned int v128;          // esi@1
  unsigned int v129;          // edi@1
  unsigned int v130;          // edi@1
  int v131;                   // ST18_4@1
  unsigned int v132;          // edx@1
  unsigned int v133;          // edx@1
  int v134;                   // ST1C_4@1
  unsigned int v135;          // esi@1
  unsigned int v136;          // esi@1
  int v137;                   // ST14_4@1
  unsigned int v138;          // edi@1
  unsigned int v139;          // edi@1
  int v140;                   // ST10_4@1
  unsigned int v141;          // edx@1
  unsigned int v142;          // edx@1
  unsigned int v143;          // esi@1
  unsigned int v144;          // esi@1
  int v145;                   // ST18_4@1
  unsigned int v146;          // edi@1
  unsigned int v147;          // edi@1
  int v148;                   // ST1C_4@1
  unsigned int v149;          // edx@1
  unsigned int v150;          // edx@1
  int v151;                   // ST14_4@1
  unsigned int v152;          // esi@1
  unsigned int v153;          // esi@1
  int v154;                   // ST10_4@1
  unsigned int v155;          // edi@1
  unsigned int v156;          // edi@1
  unsigned int v157;          // edx@1
  unsigned int v158;          // edx@1
  int v159;                   // ST18_4@1
  unsigned int v160;          // esi@1
  unsigned int v161;          // esi@1
  int v162;                   // ST1C_4@1
  unsigned int v163;          // edi@1
  unsigned int v164;          // edi@1
  int v165;                   // ebx@1
  unsigned int v166;          // ST14_4@1
  unsigned int v167;          // edx@1
  unsigned int v168;          // ebx@1
  int v169;                   // ST10_4@1
  unsigned int v170;          // edx@1
  SHA1_WORKSPACE_BLOCK *v171; // ebp@1
  unsigned int v172;          // esi@1
  SHA1_WORKSPACE_BLOCK *v173; // ebp@1
  unsigned int v174;          // edi@1
  int v175;                   // ST18_4@1
  unsigned int v176;          // edx@1
  unsigned int v177;          // ebx@1
  int v178;                   // ebp@1
  unsigned int v179;          // edx@1
  unsigned int v180;          // esi@1
  int v181;                   // ST14_4@1
  unsigned int v182;          // edx@1
  unsigned int v183;          // edi@1
  int v184;                   // ST10_4@1
  unsigned int v185;          // edx@1
  unsigned int v186;          // ebx@1
  unsigned int v187;          // edx@1
  SHA1_WORKSPACE_BLOCK *v188; // ebp@1
  unsigned int v189;          // esi@1
  int v190;                   // ST18_4@1
  unsigned int v191;          // edx@1
  unsigned int v192;          // edi@1
  int v193;                   // ebp@1
  unsigned int v194;          // edx@1
  unsigned int v195;          // ebx@1
  int v196;                   // ST14_4@1
  unsigned int v197;          // edx@1
  unsigned int v198;          // esi@1
  int v199;                   // ST10_4@1
  unsigned int v200;          // edx@1
  unsigned int v201;          // edi@1
  unsigned int v202;          // edx@1
  SHA1_WORKSPACE_BLOCK *v203; // ebp@1
  unsigned int v204;          // ebx@1
  int v205;                   // ST18_4@1
  unsigned int v206;          // edx@1
  unsigned int v207;          // esi@1
  int v208;                   // ebp@1
  unsigned int v209;          // edx@1
  unsigned int v210;          // edi@1
  int v211;                   // ST14_4@1
  unsigned int v212;          // edx@1
  unsigned int v213;          // ebx@1
  int v214;                   // ST10_4@1
  unsigned int v215;          // edx@1
  unsigned int v216;          // esi@1
  unsigned int v217;          // edx@1
  SHA1_WORKSPACE_BLOCK *v218; // ebp@1
  unsigned int v219;          // edi@1
  int v220;                   // ST18_4@1
  unsigned int v221;          // edx@1
  unsigned int v222;          // ebx@1
  int v223;                   // ebp@1
  unsigned int v224;          // edx@1
  unsigned int *v225;         // edx@1
  int v226;                   // esi@1
  unsigned int v227;          // edx@1
  unsigned int v228;          // edx@1
  unsigned int v229;          // edi@1
  unsigned int v230;          // eax@1
  unsigned int v231;          // edx@1
  int statea;                 // [sp+24h] [bp+4h]@1
  int stateb;                 // [sp+24h] [bp+4h]@1
  int statec;                 // [sp+24h] [bp+4h]@1
  int stated;                 // [sp+24h] [bp+4h]@1
  int statee;                 // [sp+24h] [bp+4h]@1
  int statef;                 // [sp+24h] [bp+4h]@1
  int stateg;                 // [sp+24h] [bp+4h]@1
  int stateh;                 // [sp+24h] [bp+4h]@1
  int statei;                 // [sp+24h] [bp+4h]@1
  int statej;                 // [sp+24h] [bp+4h]@1
  int statek;                 // [sp+24h] [bp+4h]@1
  int statel;                 // [sp+24h] [bp+4h]@1
  int statem;                 // [sp+24h] [bp+4h]@1
  int staten;                 // [sp+24h] [bp+4h]@1
  int stateo;                 // [sp+24h] [bp+4h]@1
  unsigned int *statep;       // [sp+24h] [bp+4h]@1
  unsigned int b;             // [sp+28h] [bp+8h]@1
  unsigned int ba;            // [sp+28h] [bp+8h]@1
  unsigned int bb;            // [sp+28h] [bp+8h]@1
  unsigned int bc;            // [sp+28h] [bp+8h]@1
  unsigned int bd;            // [sp+28h] [bp+8h]@1
  unsigned int be;            // [sp+28h] [bp+8h]@1
  unsigned int bf;            // [sp+28h] [bp+8h]@1
  unsigned int bg;            // [sp+28h] [bp+8h]@1
  unsigned int bh;            // [sp+28h] [bp+8h]@1
  unsigned int bi;            // [sp+28h] [bp+8h]@1
  unsigned int bj;            // [sp+28h] [bp+8h]@1
  unsigned int bk;            // [sp+28h] [bp+8h]@1
  unsigned int bl;            // [sp+28h] [bp+8h]@1
  unsigned int bm;            // [sp+28h] [bp+8h]@1
  unsigned int bn;            // [sp+28h] [bp+8h]@1
  unsigned int bo;            // [sp+28h] [bp+8h]@1
  unsigned int bp;            // [sp+28h] [bp+8h]@1
  unsigned int bq;            // [sp+28h] [bp+8h]@1
  unsigned int br;            // [sp+28h] [bp+8h]@1
  unsigned int bs;            // [sp+28h] [bp+8h]@1
  unsigned int bt;            // [sp+28h] [bp+8h]@1
  unsigned int bu;            // [sp+28h] [bp+8h]@1
  unsigned int bv;            // [sp+28h] [bp+8h]@1
  unsigned int bw;            // [sp+28h] [bp+8h]@1
  unsigned int bx;            // [sp+28h] [bp+8h]@1
  unsigned int by;            // [sp+28h] [bp+8h]@1
  unsigned int bz;            // [sp+28h] [bp+8h]@1
  unsigned int bba;           // [sp+28h] [bp+8h]@1
  unsigned int bbb;           // [sp+28h] [bp+8h]@1
  unsigned int bbc;           // [sp+28h] [bp+8h]@1
  unsigned int bbd;           // [sp+28h] [bp+8h]@1
  unsigned int bbe;           // [sp+28h] [bp+8h]@1
  unsigned int bbf;           // [sp+28h] [bp+8h]@1
  unsigned int bbg;           // [sp+28h] [bp+8h]@1
  unsigned int bbh;           // [sp+28h] [bp+8h]@1
  unsigned int bbi;           // [sp+28h] [bp+8h]@1
  unsigned int bbj;           // [sp+28h] [bp+8h]@1
  unsigned int bbk;           // [sp+28h] [bp+8h]@1
  unsigned int bbl;           // [sp+28h] [bp+8h]@1
  unsigned int bbm;           // [sp+28h] [bp+8h]@1
  unsigned int bbn;           // [sp+28h] [bp+8h]@1
  unsigned int bbo;           // [sp+28h] [bp+8h]@1
  unsigned int bbp;           // [sp+28h] [bp+8h]@1
  unsigned int bbq;           // [sp+28h] [bp+8h]@1
  unsigned int bbr;           // [sp+28h] [bp+8h]@1
  unsigned int bbs;           // [sp+28h] [bp+8h]@1
  unsigned int bbt;           // [sp+28h] [bp+8h]@1
  unsigned int bbu;           // [sp+28h] [bp+8h]@1
  unsigned int bbv;           // [sp+28h] [bp+8h]@1
  unsigned int bbw;           // [sp+28h] [bp+8h]@1
  unsigned int bbx;           // [sp+28h] [bp+8h]@1
  unsigned int bby;           // [sp+28h] [bp+8h]@1
  unsigned int bbz;           // [sp+28h] [bp+8h]@1
  unsigned int bca;           // [sp+28h] [bp+8h]@1
  unsigned int bcb;           // [sp+28h] [bp+8h]@1
  unsigned int bcc;           // [sp+28h] [bp+8h]@1

  v3 = Source;
  int s = sizeof(SHA1_WORKSPACE_BLOCK);
  memcpy(v3->block, buffer,64);
  v4 = Source->state;
  v5 = *Source->state;
  c = Source->state[2];
  d = Source->state[3];
  e = Source->state[4];
  v9 = Source->state[1];
  memcpy(Source->block, buffer, 64);
#if 0
  Source->block = (SHA1_WORKSPACE_BLOCK*)((((*(DWORD*)Source->block & 0xFF00) | (*(DWORD *)Source->block << 16)) << 8) |
	  (((*(DWORD *)Source->block >> 16) | *(DWORD *)Source->block & 0xFF0000u) >> 8));

#endif // 0

  


  v10 = Source->block;
  v11 =e + Source->block->l[0] + (d ^ v9 & (c ^ d)) + (32 * v5 | (v5 >> 27)) + 1518500249;
  statea = (v9 >> 2) | (v9 << 30);
  Source->block->l[1] = ((Source->block->l[1] & 0xFF00 | (Source->block->l[1] << 16)) << 8) |
                    (((Source->block->l[1] >> 16) | Source->block->l[1] & 0xFF0000) >> 8);

  b = (unsigned int)Source->block;

  v12 = d + *(DWORD *)(b + 4) + (c ^ v5 & (statea ^ c)) +(32 * v11 | ((unsigned __int64)v11 >> 27)) + 1518500249;
  a = (v5 >> 2) | (v5 << 30);
  *(DWORD *)(b + 8) =
      ((*(DWORD *)(b + 8) & 0xFF00 | (*(DWORD *)(b + 8) << 16)) << 8) |
      (((*(DWORD *)(b + 8) >> 16) | *(DWORD *)(b + 8) & 0xFF0000u) >> 8);

  ba = (unsigned int)v3->block;
  v14 = c + *(DWORD *)(ba + 8) + (statea ^ v11 & (a ^ statea)) +
        (32 * v12 | ((unsigned __int64)v12 >> 27)) + 1518500249;

  v15 = (v11 >> 2) | (v11 << 30);
  *(DWORD *)(ba + 12) =
      ((*(DWORD *)(ba + 12) & 0xFF00 | (*(DWORD *)(ba + 12) << 16)) << 8) |
      (((*(DWORD *)(ba + 12) >> 16) | *(DWORD *)(ba + 12) & 0xFF0000u) >> 8);

  bb = (unsigned int)v3->block;
  v16 = statea + *(DWORD *)(bb + 12) + (a ^ v12 & (a ^ v15)) +
        (32 * v14 | ((unsigned __int64)v14 >> 27)) + 1518500249;

  v17 = (v12 >> 2) | (v12 << 30);
  *(DWORD *)(bb + 16) =
      ((*(DWORD *)(bb + 16) & 0xFF00 | (*(DWORD *)(bb + 16) << 16)) << 8) |
      (((*(DWORD *)(bb + 16) >> 16) | *(DWORD *)(bb + 16) & 0xFF0000u) >> 8);
  bc = (unsigned int)v3->block;


  v18 = a + *(DWORD *)(bc + 16) + (v15 ^ v14 & (v17 ^ v15)) +
        (32 * v16 | ((unsigned __int64)v16 >> 27)) + 1518500249;
  v19 = (v14 >> 2) | (v14 << 30);


  *(DWORD *)(bc + 20) =
      ((*(DWORD *)(bc + 20) & 0xFF00 | (*(DWORD *)(bc + 20) << 16)) << 8) |
      (((*(DWORD *)(bc + 20) >> 16) | *(DWORD *)(bc + 20) & 0xFF0000u) >> 8);
  bd = (unsigned int)v3->block;


  v20 = v15 + *(DWORD *)(bd + 20) + (v17 ^ v16 & (v19 ^ v17)) +
        (32 * v18 | ((unsigned __int64)v18 >> 27)) + 1518500249;
  stateb = (v16 >> 2) | (v16 << 30);


  *(DWORD *)(bd + 24) =
      ((*(DWORD *)(bd + 24) & 0xFF00 | (*(DWORD *)(bd + 24) << 16)) << 8) |
      (((*(DWORD *)(bd + 24) >> 16) | *(DWORD *)(bd + 24) & 0xFF0000u) >> 8);
  be = (unsigned int)v3->block;


  v21 = v17 + *(DWORD *)(be + 24) + (v19 ^ v18 & (stateb ^ v19)) +
        (32 * v20 | ((unsigned __int64)v20 >> 27)) + 1518500249;
  v22 = (v18 >> 2) | (v18 << 30);


  *(DWORD *)(be + 28) =
      ((*(DWORD *)(be + 28) & 0xFF00 | (*(DWORD *)(be + 28) << 16)) << 8) |
      (((*(DWORD *)(be + 28) >> 16) | *(DWORD *)(be + 28) & 0xFF0000u) >> 8);
  bf = (unsigned int)v3->block;


  v23 = v19 + *(DWORD *)(bf + 28) + (stateb ^ v20 & (v22 ^ stateb)) +
        (32 * v21 | ((unsigned __int64)v21 >> 27)) + 1518500249;
  v24 = (v20 >> 2) | (v20 << 30);


  *(DWORD *)(bf + 32) =
      ((*(DWORD *)(bf + 32) & 0xFF00 | (*(DWORD *)(bf + 32) << 16)) << 8) |
      (((*(DWORD *)(bf + 32) >> 16) | *(DWORD *)(bf + 32) & 0xFF0000u) >> 8);
  bg = (unsigned int)v3->block;


  v25 = stateb + *(DWORD *)(bg + 32) + (v22 ^ v21 & (v22 ^ v24)) +
        (32 * v23 | ((unsigned __int64)v23 >> 27)) + 1518500249;
  v26 = (v21 >> 2) | (v21 << 30);


  *(DWORD *)(bg + 36) =
      ((*(DWORD *)(bg + 36) & 0xFF00 | (*(DWORD *)(bg + 36) << 16)) << 8) |
      (((*(DWORD *)(bg + 36) >> 16) | *(DWORD *)(bg + 36) & 0xFF0000u) >> 8);
  bh = (unsigned int)v3->block;


  v27 = v22 + *(DWORD *)(bh + 36) + (v24 ^ v23 & (v26 ^ v24)) +
        (32 * v25 | ((unsigned __int64)v25 >> 27)) + 1518500249;
  v28 = (v23 >> 2) | (v23 << 30);


  *(DWORD *)(bh + 40) =
      ((*(DWORD *)(bh + 40) & 0xFF00 | (*(DWORD *)(bh + 40) << 16)) << 8) |
      (((*(DWORD *)(bh + 40) >> 16) | *(DWORD *)(bh + 40) & 0xFF0000u) >> 8);
  bi = (unsigned int)v3->block;

  v29 = v24 + *(DWORD *)(bi + 40) + (v26 ^ v25 & (v28 ^ v26)) +
        (32 * v27 | ((unsigned __int64)v27 >> 27)) + 1518500249;
  statec = (v25 >> 2) | (v25 << 30);

  *(DWORD *)(bi + 44) =
      ((*(DWORD *)(bi + 44) & 0xFF00 | (*(DWORD *)(bi + 44) << 16)) << 8) |
      (((*(DWORD *)(bi + 44) >> 16) | *(DWORD *)(bi + 44) & 0xFF0000u) >> 8);
  bj = (unsigned int)v3->block;

  v30 = v26 + *(DWORD *)(bj + 44) + (v28 ^ v27 & (statec ^ v28)) +
        (32 * v29 | ((unsigned __int64)v29 >> 27)) + 1518500249;
  v31 = (v27 >> 2) | (v27 << 30);


  *(DWORD *)(bj + 48) =
      ((*(DWORD *)(bj + 48) & 0xFF00 | (*(DWORD *)(bj + 48) << 16)) << 8) |
      (((*(DWORD *)(bj + 48) >> 16) | *(DWORD *)(bj + 48) & 0xFF0000u) >> 8);
  bk = (unsigned int)v3->block;


  v32 = v28 + *(DWORD *)(bk + 48) + (statec ^ v29 & (v31 ^ statec)) +
        (32 * v30 | ((unsigned __int64)v30 >> 27)) + 1518500249;
  v33 = (v29 >> 2) | (v29 << 30);

  *(DWORD *)(bk + 52) =
      ((*(DWORD *)(bk + 52) & 0xFF00 | (*(DWORD *)(bk + 52) << 16)) << 8) |
      (((*(DWORD *)(bk + 52) >> 16) | *(DWORD *)(bk + 52) & 0xFF0000u) >> 8);
  bl = (unsigned int)v3->block;

  v34 = statec + *(DWORD *)(bl + 52) + (v31 ^ v30 & (v31 ^ v33)) +
        (32 * v32 | ((unsigned __int64)v32 >> 27)) + 1518500249;
  v35 = (v30 >> 2) | (v30 << 30);


  v36 = v35;
  *(DWORD *)(bl + 56) =
      ((*(DWORD *)(bl + 56) & 0xFF00 | (*(DWORD *)(bl + 56) << 16)) << 8) |
      (((*(DWORD *)(bl + 56) >> 16) | *(DWORD *)(bl + 56) & 0xFF0000u) >> 8);
  bm = (unsigned int)v3->block;


  v37 = v31 + *(DWORD *)(bm + 56) + (v33 ^ v32 & (v35 ^ v33)) +
        (32 * v34 | ((unsigned __int64)v34 >> 27)) + 1518500249;
  v38 = (v32 >> 2) | (v32 << 30);


  *(DWORD *)(bm + 60) =
      ((*(DWORD *)(bm + 60) & 0xFF00 | (*(DWORD *)(bm + 60) << 16)) << 8) |
      (((*(DWORD *)(bm + 60) >> 16) | *(DWORD *)(bm + 60) & 0xFF0000u) >> 8);
  bn = (unsigned int)v3->block;


  v39 = v33 + *(DWORD *)(bn + 60) + (v36 ^ v34 & (v38 ^ v36)) +
        (32 * v37 | ((unsigned __int64)v37 >> 27)) + 1518500249;
  stated = (v34 >> 2) | (v34 << 30);

  *(DWORD *)bn =
      2 * (*(DWORD *)bn ^ *(DWORD *)(bn + 8) ^ *(DWORD *)(bn + 32) ^
           *(DWORD *)(bn + 52)) |
      ((*(DWORD *)bn ^ *(DWORD *)(bn + 8) ^
        (unsigned int)(*(DWORD *)(bn + 32) ^ *(DWORD *)(bn + 52))) >>
       31);
  bo = (unsigned int)v3->block;

  v40 = v36 + *(DWORD *)bo + (v38 ^ v37 & (stated ^ v38)) +
        (32 * v39 | ((unsigned __int64)v39 >> 27)) + 1518500249;
  v41 = (v37 >> 2) | (v37 << 30);

  v42 = *(DWORD *)(bo + 4) ^ *(DWORD *)(bo + 12) ^ *(DWORD *)(bo + 36) ^
        *(DWORD *)(bo + 56);
  *(DWORD *)(bo + 4) = 2 * v42 | (v42 >> 31);

  bp = (unsigned int)v3->block;
  v43 = v38 + *(DWORD *)(bp + 4) + (stated ^ v39 & (v41 ^ stated)) +
        (32 * v40 | ((unsigned __int64)v40 >> 27)) + 1518500249;
  v44 = (v39 >> 2) | (v39 << 30);

  v45 = *(DWORD *)(bp + 8) ^ *(DWORD *)(bp + 16) ^ *(DWORD *)(bp + 40) ^
        *(DWORD *)(bp + 60);
  *(DWORD *)(bp + 8) = 2 * v45 | (v45 >> 31);

  bq = (unsigned int)v3->block;
  v46 = stated + *(DWORD *)(bq + 8) + (v41 ^ v40 & (v41 ^ v44)) +
        (32 * v43 | ((unsigned __int64)v43 >> 27)) + 1518500249;

  v47 = (v40 >> 2) | (v40 << 30);
  v48 = *(DWORD *)(bq + 12) ^ *(DWORD *)bq ^ *(DWORD *)(bq + 20) ^
        *(DWORD *)(bq + 44);

  *(DWORD *)(bq + 12) = 2 * v48 | (v48 >> 31);
  br = (unsigned int)v3->block;
  v49 = v41 + *(DWORD *)(br + 12) + (v44 ^ v43 & (v47 ^ v44)) +
        (32 * v46 | ((unsigned __int64)v46 >> 27)) + 1518500249;

  v50 = (v43 >> 2) | (v43 << 30);
  v51 = *(DWORD *)(br + 16) ^ *(DWORD *)(br + 4) ^ *(DWORD *)(br + 24) ^
        *(DWORD *)(br + 48);

  *(DWORD *)(br + 16) = 2 * v51 | (v51 >> 31);
  v52 = v3->block;

  v53 = v44 + v52->l[4] + (v46 ^ v50 ^ v47) +
        (32 * v49 | ((unsigned __int64)v49 >> 27)) + 1859775393;
  statee = (v46 >> 2) | (v46 << 30);
  v54 = v52->l[5] ^ v52->l[2] ^ v52->l[7] ^ v52->l[13];
  v52->l[5] = 2 * v54 | (v54 >> 31);
  v55 = v3->block;
  v56 = v47 + v55->l[5] + (v49 ^ statee ^ v50) +
        (32 * v53 | ((unsigned __int64)v53 >> 27)) + 1859775393;
  v57 = (v49 >> 2) | (v49 << 30);
  v58 = v55->l[6] ^ v55->l[3] ^ v55->l[8] ^ v55->l[14];
  v55->l[6] = 2 * v58 | (v58 >> 31);
  bs = (unsigned int)v3->block;
  v59 = *(DWORD *)(bs + 24) + (v57 ^ statee ^ v53) +
        (32 * v56 | ((unsigned __int64)v56 >> 27)) + v50 + 1859775393;
  v60 = (v53 >> 2) | (v53 << 30);
  v61 = *(DWORD *)(bs + 28) ^ *(DWORD *)(bs + 16) ^ *(DWORD *)(bs + 36) ^
        *(DWORD *)(bs + 60);
  *(DWORD *)(bs + 28) = 2 * v61 | (v61 >> 31);
  v62 = statee + v3->block->l[7] + (v57 ^ v56 ^ v60) +
        (32 * v59 | ((unsigned __int64)v59 >> 27)) + 1859775393;
  v63 = (v56 >> 2) | (v56 << 30);
  v64 = v3->block->l[8] ^ *(DWORD *)v3->block ^ v3->block->l[5] ^
        v3->block->l[10];
  v3->block->l[8] = 2 * v64 | (v64 >> 31);
  v65 = v57 + v3->block->l[8] + (v59 ^ v63 ^ v60) +
        (32 * v62 | ((unsigned __int64)v62 >> 27)) + 1859775393;
  v66 = (v59 >> 2) | (v59 << 30);
  v67 = v3->block->l[9] ^ v3->block->l[1] ^ v3->block->l[6] ^ v3->block->l[11];
  v3->block->l[9] = 2 * v67 | (v67 >> 31);
  bt = (unsigned int)v3->block;
  v68 = *(DWORD *)(bt + 36) + (v62 ^ v66 ^ v63) +
        (32 * v65 | ((unsigned __int64)v65 >> 27)) + v60 + 1859775393;
  statef = (v62 >> 2) | (v62 << 30);
  v69 = *(DWORD *)(bt + 40) ^ *(DWORD *)(bt + 8) ^ *(DWORD *)(bt + 28) ^
        *(DWORD *)(bt + 48);
  *(DWORD *)(bt + 40) = 2 * v69 | (v69 >> 31);
  v70 = v3->block;
  v71 = v63 + v70->l[10] + (v65 ^ statef ^ v66) +
        (32 * v68 | ((unsigned __int64)v68 >> 27)) + 1859775393;
  v72 = (v65 >> 2) | (v65 << 30);
  v73 = v70->l[11] ^ v70->l[3] ^ v70->l[8] ^ v70->l[13];
  v70->l[11] = 2 * v73 | (v73 >> 31);
  bu = (unsigned int)v3->block;
  v74 = *(DWORD *)(bu + 44) + (v72 ^ statef ^ v68) +
        (32 * v71 | ((unsigned __int64)v71 >> 27)) + v66 + 1859775393;
  v75 = (v68 >> 2) | (v68 << 30);
  v76 = *(DWORD *)(bu + 48) ^ *(DWORD *)(bu + 16) ^ *(DWORD *)(bu + 36) ^
        *(DWORD *)(bu + 56);
  *(DWORD *)(bu + 48) = 2 * v76 | (v76 >> 31);
  v77 = statef + v3->block->l[12] + (v72 ^ v71 ^ v75) +
        (32 * v74 | ((unsigned __int64)v74 >> 27)) + 1859775393;
  v78 = (v71 >> 2) | (v71 << 30);
  v79 =
      v3->block->l[13] ^ v3->block->l[5] ^ v3->block->l[10] ^ v3->block->l[15];
  v3->block->l[13] = 2 * v79 | (v79 >> 31);
  v80 = v72 + v3->block->l[13] + (v74 ^ v78 ^ v75) +
        (32 * v77 | ((unsigned __int64)v77 >> 27)) + 1859775393;
  v81 = (v74 >> 2) | (v74 << 30);
  v82 = *(DWORD *)v3->block ^ v3->block->l[14] ^ v3->block->l[6] ^
        v3->block->l[11];
  v3->block->l[14] = 2 * v82 | (v82 >> 31);
  bv = (unsigned int)v3->block;
  v83 = *(DWORD *)(bv + 56) + (v77 ^ v81 ^ v78) +
        (32 * v80 | ((unsigned __int64)v80 >> 27)) + v75 + 1859775393;
  stateg = (v77 >> 2) | (v77 << 30);
  v84 = *(DWORD *)(bv + 60) ^ *(DWORD *)(bv + 4) ^ *(DWORD *)(bv + 28) ^
        *(DWORD *)(bv + 48);
  *(DWORD *)(bv + 60) = 2 * v84 | (v84 >> 31);
  v85 = v3->block;
  v86 = v78 + v85->l[15] + (v80 ^ stateg ^ v81) +
        (32 * v83 | ((unsigned __int64)v83 >> 27)) + 1859775393;
  v87 = (v80 >> 2) | (v80 << 30);
  v88 = v85->l[0] ^ v85->l[2] ^ v85->l[8] ^ v85->l[13];
  v85->l[0] = 2 * v88 | (v88 >> 31);
  bw = (unsigned int)v3->block;
  v89 = *(DWORD *)bw + (v87 ^ stateg ^ v83) +
        (32 * v86 | ((unsigned __int64)v86 >> 27)) + v81 + 1859775393;
  v90 = (v83 >> 2) | (v83 << 30);
  v91 = *(DWORD *)(bw + 4) ^ *(DWORD *)(bw + 12) ^ *(DWORD *)(bw + 36) ^
        *(DWORD *)(bw + 56);
  *(DWORD *)(bw + 4) = 2 * v91 | (v91 >> 31);
  v92 = stateg + v3->block->l[1] + (v87 ^ v86 ^ v90) +
        (32 * v89 | ((unsigned __int64)v89 >> 27)) + 1859775393;
  v93 = (v86 >> 2) | (v86 << 30);
  v94 = v3->block->l[2] ^ v3->block->l[4] ^ v3->block->l[10] ^
        v3->block->l[15];
  v3->block->l[2] = 2 * v94 | (v94 >> 31);
  v95 = v87 + v3->block->l[2] + (v89 ^ v93 ^ v90) +
        (32 * v92 | ((unsigned __int64)v92 >> 27)) + 1859775393;
  v96 = (v89 >> 2) | (v89 << 30);
  v97 = v3->block->l[3] ^ *(DWORD *)v3->block ^ v3->block->l[5] ^
        v3->block->l[11];
  v3->block->l[3] = 2 * v97 | (v97 >> 31);
  bx = (unsigned int)v3->block;
  v98 = *(DWORD *)(bx + 12) + (v92 ^ v96 ^ v93) +
        (32 * v95 | ((unsigned __int64)v95 >> 27)) + v90 + 1859775393;
  stateh = (v92 >> 2) | (v92 << 30);
  v99 = *(DWORD *)(bx + 16) ^ *(DWORD *)(bx + 4) ^ *(DWORD *)(bx + 24) ^
        *(DWORD *)(bx + 48);
  *(DWORD *)(bx + 16) = 2 * v99 | (v99 >> 31);
  v100 = v3->block;
  v101 = v93 + v100->l[4] + (v95 ^ stateh ^ v96) +
         (32 * v98 | ((unsigned __int64)v98 >> 27)) + 1859775393;
  v102 = (v95 >> 2) | (v95 << 30);
  v103 = v100->l[5] ^ v100->l[2] ^ v100->l[7] ^ v100->l[13];
  v100->l[5] = 2 * v103 | (v103 >> 31);
  by = (unsigned int)v3->block;
  v104 = v96 + *(DWORD *)(by + 20) + (v102 ^ stateh ^ v98) +
         (32 * v101 | ((unsigned __int64)v101 >> 27)) + 1859775393;
  v105 = (v98 >> 2) | (v98 << 30);
  v106 = *(DWORD *)(by + 24) ^ *(DWORD *)(by + 12) ^ *(DWORD *)(by + 32) ^
         *(DWORD *)(by + 56);
  *(DWORD *)(by + 24) = 2 * v106 | (v106 >> 31);
  bz = (unsigned int)v3->block;
  v107 = *(DWORD *)(bz + 24) + (v102 ^ v101 ^ v105) +
         (32 * v104 | ((unsigned __int64)v104 >> 27)) + stateh + 1859775393;
  v108 = (v101 >> 2) | (v101 << 30);
  v109 = *(DWORD *)(bz + 28) ^ *(DWORD *)(bz + 16) ^ *(DWORD *)(bz + 36) ^
         *(DWORD *)(bz + 60);
  *(DWORD *)(bz + 28) = 2 * v109 | (v109 >> 31);
  bba = (unsigned int)v3->block;
  v110 = *(DWORD *)(bba + 28) + (v104 ^ v108 ^ v105) +
         (32 * v107 | ((unsigned __int64)v107 >> 27)) + v102 + 1859775393;
  v111 = (v104 >> 2) | (v104 << 30);
  v112 = *(DWORD *)(bba + 32) ^ *(DWORD *)bba ^ *(DWORD *)(bba + 20) ^
         *(DWORD *)(bba + 40);
  *(DWORD *)(bba + 32) = 2 * v112 | (v112 >> 31);
  bbb = (unsigned int)v3->block;
  v113 = v105 + *(DWORD *)(bbb + 32) +
         (32 * v110 | ((unsigned __int64)v110 >> 27)) +
         (v107 & v111 | v108 & (v107 | v111)) - 1894007588;
  v114 = (v107 >> 2) | (v107 << 30);
  v115 = *(DWORD *)(bbb + 36) ^ *(DWORD *)(bbb + 4) ^ *(DWORD *)(bbb + 24) ^
         *(DWORD *)(bbb + 44);
  *(DWORD *)(bbb + 36) = 2 * v115 | (v115 >> 31);
  bbc = (unsigned int)v3->block;
  v116 = *(DWORD *)(bbc + 36) + (32 * v113 | ((unsigned __int64)v113 >> 27)) +
         (v110 & v114 | v111 & (v110 | v114)) + v108 - 1894007588;
  v117 = (v110 >> 2) | (v110 << 30);
  v118 = *(DWORD *)(bbc + 40) ^ *(DWORD *)(bbc + 8) ^ *(DWORD *)(bbc + 28) ^
         *(DWORD *)(bbc + 48);
  *(DWORD *)(bbc + 40) = 2 * v118 | (v118 >> 31);
  bbd = (unsigned int)v3->block;
  v119 = v111 + *(DWORD *)(bbd + 40) +
         (32 * v116 | ((unsigned __int64)v116 >> 27)) +
         (v117 & v113 | v114 & (v117 | v113)) - 1894007588;
  v120 = (v113 >> 2) | (v113 << 30);
  v121 = *(DWORD *)(bbd + 44) ^ *(DWORD *)(bbd + 12) ^ *(DWORD *)(bbd + 32) ^
         *(DWORD *)(bbd + 52);
  *(DWORD *)(bbd + 44) = 2 * v121 | (v121 >> 31);
  bbe = (unsigned int)v3->block;
  v122 = v114 + *(DWORD *)(bbe + 44) +
         (32 * v119 | ((unsigned __int64)v119 >> 27)) +
         (v116 & v120 | v117 & (v116 | v120)) - 1894007588;
  v123 = (v116 >> 2) | (v116 << 30);
  v124 = *(DWORD *)(bbe + 48) ^ *(DWORD *)(bbe + 16) ^ *(DWORD *)(bbe + 36) ^
         *(DWORD *)(bbe + 56);
  *(DWORD *)(bbe + 48) = 2 * v124 | (v124 >> 31);
  bbf = (unsigned int)v3->block;
  v125 = v117 + *(DWORD *)(bbf + 48) +
         (32 * v122 | ((unsigned __int64)v122 >> 27)) +
         (v119 & v123 | v120 & (v119 | v123)) - 1894007588;
  v126 = (v119 >> 2) | (v119 << 30);
  v127 = *(DWORD *)(bbf + 52) ^ *(DWORD *)(bbf + 20) ^ *(DWORD *)(bbf + 40) ^
         *(DWORD *)(bbf + 60);
  *(DWORD *)(bbf + 52) = 2 * v127 | (v127 >> 31);
  bbg = (unsigned int)v3->block;
  v128 = v120 + *(DWORD *)(bbg + 52) +
         (32 * v125 | ((unsigned __int64)v125 >> 27)) +
         (v122 & v126 | v123 & (v122 | v126)) - 1894007588;
  statei = (v122 >> 2) | (v122 << 30);
  v129 = *(DWORD *)bbg ^ *(DWORD *)(bbg + 56) ^ *(DWORD *)(bbg + 24) ^
         *(DWORD *)(bbg + 44);
  *(DWORD *)(bbg + 56) = 2 * v129 | (v129 >> 31);
  bbh = (unsigned int)v3->block;
  v130 = v123 + *(DWORD *)(bbh + 56) +
         (32 * v128 | ((unsigned __int64)v128 >> 27)) +
         (v125 & statei | v126 & (v125 | statei)) - 1894007588;
  v131 = (v125 >> 2) | (v125 << 30);
  v132 = *(DWORD *)(bbh + 60) ^ *(DWORD *)(bbh + 4) ^ *(DWORD *)(bbh + 28) ^
         *(DWORD *)(bbh + 48);
  *(DWORD *)(bbh + 60) = 2 * v132 | (v132 >> 31);
  bbi = (unsigned int)v3->block;
  v133 = v126 + *(DWORD *)(bbi + 60) +
         (32 * v130 | ((unsigned __int64)v130 >> 27)) +
         (v131 & v128 | statei & (v131 | v128)) - 1894007588;
  v134 = (v128 >> 2) | (v128 << 30);
  v135 = *(DWORD *)bbi ^ *(DWORD *)(bbi + 8) ^ *(DWORD *)(bbi + 32) ^
         *(DWORD *)(bbi + 52);
  *(DWORD *)bbi = 2 * v135 | (v135 >> 31);
  bbj = (unsigned int)v3->block;
  v136 = statei + *(DWORD *)bbj +
         (32 * v133 | ((unsigned __int64)v133 >> 27)) +
         (v130 & v134 | v131 & (v130 | v134)) - 1894007588;
  v137 = (v130 >> 2) | (v130 << 30);
  v138 = *(DWORD *)(bbj + 4) ^ *(DWORD *)(bbj + 12) ^ *(DWORD *)(bbj + 36) ^
         *(DWORD *)(bbj + 56);
  *(DWORD *)(bbj + 4) = 2 * v138 | (v138 >> 31);
  bbk = (unsigned int)v3->block;
  v139 = v131 + *(DWORD *)(bbk + 4) +
         (32 * v136 | ((unsigned __int64)v136 >> 27)) +
         (v133 & v137 | v134 & (v133 | v137)) - 1894007588;
  v140 = (v133 >> 2) | (v133 << 30);
  v141 = *(DWORD *)(bbk + 8) ^ *(DWORD *)(bbk + 16) ^ *(DWORD *)(bbk + 40) ^
         *(DWORD *)(bbk + 60);
  *(DWORD *)(bbk + 8) = 2 * v141 | (v141 >> 31);
  bbl = (unsigned int)v3->block;
  v142 = v134 + *(DWORD *)(bbl + 8) +
         (32 * v139 | ((unsigned __int64)v139 >> 27)) +
         (v136 & v140 | v137 & (v136 | v140)) - 1894007588;
  statej = (v136 >> 2) | (v136 << 30);
  v143 = *(DWORD *)(bbl + 12) ^ *(DWORD *)bbl ^ *(DWORD *)(bbl + 20) ^
         *(DWORD *)(bbl + 44);
  *(DWORD *)(bbl + 12) = 2 * v143 | (v143 >> 31);
  bbm = (unsigned int)v3->block;
  v144 = v137 + *(DWORD *)(bbm + 12) +
         (32 * v142 | ((unsigned __int64)v142 >> 27)) +
         (v139 & statej | v140 & (v139 | statej)) - 1894007588;
  v145 = (v139 >> 2) | (v139 << 30);
  v146 = *(DWORD *)(bbm + 16) ^ *(DWORD *)(bbm + 4) ^ *(DWORD *)(bbm + 24) ^
         *(DWORD *)(bbm + 48);
  *(DWORD *)(bbm + 16) = 2 * v146 | (v146 >> 31);
  bbn = (unsigned int)v3->block;
  v147 = v140 + *(DWORD *)(bbn + 16) +
         (32 * v144 | ((unsigned __int64)v144 >> 27)) +
         (v145 & v142 | statej & (v145 | v142)) - 1894007588;
  v148 = (v142 >> 2) | (v142 << 30);
  v149 = *(DWORD *)(bbn + 20) ^ *(DWORD *)(bbn + 8) ^ *(DWORD *)(bbn + 28) ^
         *(DWORD *)(bbn + 52);
  *(DWORD *)(bbn + 20) = 2 * v149 | (v149 >> 31);
  bbo = (unsigned int)v3->block;
  v150 = statej + *(DWORD *)(bbo + 20) +
         (32 * v147 | ((unsigned __int64)v147 >> 27)) +
         (v144 & v148 | v145 & (v144 | v148)) - 1894007588;
  v151 = (v144 >> 2) | (v144 << 30);
  v152 = *(DWORD *)(bbo + 24) ^ *(DWORD *)(bbo + 12) ^ *(DWORD *)(bbo + 32) ^
         *(DWORD *)(bbo + 56);
  *(DWORD *)(bbo + 24) = 2 * v152 | (v152 >> 31);
  bbp = (unsigned int)v3->block;
  v153 = v145 + *(DWORD *)(bbp + 24) +
         (32 * v150 | ((unsigned __int64)v150 >> 27)) +
         (v147 & v151 | v148 & (v147 | v151)) - 1894007588;
  v154 = (v147 >> 2) | (v147 << 30);
  v155 = *(DWORD *)(bbp + 28) ^ *(DWORD *)(bbp + 16) ^ *(DWORD *)(bbp + 36) ^
         *(DWORD *)(bbp + 60);
  *(DWORD *)(bbp + 28) = 2 * v155 | (v155 >> 31);
  bbq = (unsigned int)v3->block;
  v156 = v148 + *(DWORD *)(bbq + 28) +
         (32 * v153 | ((unsigned __int64)v153 >> 27)) +
         (v150 & v154 | v151 & (v150 | v154)) - 1894007588;
  statek = (v150 >> 2) | (v150 << 30);
  v157 = *(DWORD *)(bbq + 32) ^ *(DWORD *)bbq ^ *(DWORD *)(bbq + 20) ^
         *(DWORD *)(bbq + 40);
  *(DWORD *)(bbq + 32) = 2 * v157 | (v157 >> 31);
  bbr = (unsigned int)v3->block;
  v158 = v151 + *(DWORD *)(bbr + 32) +
         (32 * v156 | ((unsigned __int64)v156 >> 27)) +
         (v153 & statek | v154 & (v153 | statek)) - 1894007588;
  v159 = (v153 >> 2) | (v153 << 30);
  v160 = *(DWORD *)(bbr + 36) ^ *(DWORD *)(bbr + 4) ^ *(DWORD *)(bbr + 24) ^
         *(DWORD *)(bbr + 44);
  *(DWORD *)(bbr + 36) = 2 * v160 | (v160 >> 31);
  bbs = (unsigned int)v3->block;
  v161 = v154 + *(DWORD *)(bbs + 36) +
         (32 * v158 | ((unsigned __int64)v158 >> 27)) +
         (v159 & v156 | statek & (v159 | v156)) - 1894007588;
  v162 = (v156 >> 2) | (v156 << 30);
  v163 = *(DWORD *)(bbs + 40) ^ *(DWORD *)(bbs + 8) ^ *(DWORD *)(bbs + 28) ^
         *(DWORD *)(bbs + 48);
  *(DWORD *)(bbs + 40) = 2 * v163 | (v163 >> 31);
  bbt = (unsigned int)v3->block;
  v164 = statek + *(DWORD *)(bbt + 40) +
         (32 * v161 | ((unsigned __int64)v161 >> 27)) +
         (v158 & v162 | v159 & (v158 | v162)) - 1894007588;
  v165 = (v158 >> 2) | (v158 << 30);
  v166 = v165;
  v167 = *(DWORD *)(bbt + 44) ^ *(DWORD *)(bbt + 12) ^ *(DWORD *)(bbt + 32) ^
         *(DWORD *)(bbt + 52);
  *(DWORD *)(bbt + 44) = 2 * v167 | (v167 >> 31);
  bbu = (unsigned int)v3->block;
  v168 = v159 + *(DWORD *)(bbu + 44) +
         (32 * v164 | ((unsigned __int64)v164 >> 27)) +
         (v161 & v165 | v162 & (v161 | v165)) - 1894007588;
  v169 = (v161 >> 2) | (v161 << 30);
  v170 = *(DWORD *)(bbu + 48) ^ *(DWORD *)(bbu + 16) ^ *(DWORD *)(bbu + 36) ^
         *(DWORD *)(bbu + 56);
  *(DWORD *)(bbu + 48) = 2 * v170 | (v170 >> 31);
  v171 = v3->block;
  v172 = v162 + v171->l[12] + (v164 ^ v169 ^ v166) +
         (32 * v168 | ((unsigned __int64)v168 >> 27)) - 899497514;
  statel = (v164 >> 2) | (v164 << 30);
  v171->l[13] = 2 * (v171->l[13] ^ v171->l[5] ^ v171->l[10] ^ v171->l[15]) |
                ((v171->l[13] ^ v171->l[5] ^ v171->l[10] ^ v171->l[15]) >> 31);
  v173 = v3->block;
  v174 = v166 + v173->l[13] + (v168 ^ statel ^ v169) +
         (32 * v172 | ((unsigned __int64)v172 >> 27)) - 899497514;
  v175 = (v168 >> 2) | (v168 << 30);
  v176 = v173->l[0] ^ v173->l[14] ^ v173->l[6] ^ v173->l[11];
  v173->l[14] = 2 * v176 | (v176 >> 31);
  bbv = (unsigned int)v3->block;
  v177 = *(DWORD *)(bbv + 56) + (v175 ^ statel ^ v172) +
         (32 * v174 | ((unsigned __int64)v174 >> 27)) + v169 - 899497514;
  v178 = (v172 >> 2) | (v172 << 30);
  v179 = *(DWORD *)(bbv + 60) ^ *(DWORD *)(bbv + 4) ^ *(DWORD *)(bbv + 28) ^
         *(DWORD *)(bbv + 48);
  *(DWORD *)(bbv + 60) = 2 * v179 | (v179 >> 31);
  v180 = statel + v3->block->l[15] + (v175 ^ v174 ^ v178) +
         (32 * v177 | ((unsigned __int64)v177 >> 27)) - 899497514;
  v181 = (v174 >> 2) | (v174 << 30);
  v182 = *(DWORD *)v3->block ^ v3->block->l[2] ^ v3->block->l[8] ^
         v3->block->l[13];
  *(DWORD *)v3->block = 2 * v182 | (v182 >> 31);
  v183 = v175 + *(DWORD *)v3->block + (v177 ^ v181 ^ v178) +
         (32 * v180 | ((unsigned __int64)v180 >> 27)) - 899497514;
  v184 = (v177 >> 2) | (v177 << 30);
  v185 = v3->block->l[1] ^ v3->block->l[3] ^ v3->block->l[9] ^
         v3->block->l[14];
  v3->block->l[1] = 2 * v185 | (v185 >> 31);
  bbw = (unsigned int)v3->block;
  v186 = *(DWORD *)(bbw + 4) + (v180 ^ v184 ^ v181) +
         (32 * v183 | ((unsigned __int64)v183 >> 27)) + v178 - 899497514;
  statem = (v180 >> 2) | (v180 << 30);
  v187 = *(DWORD *)(bbw + 8) ^ *(DWORD *)(bbw + 16) ^ *(DWORD *)(bbw + 40) ^
         *(DWORD *)(bbw + 60);
  *(DWORD *)(bbw + 8) = 2 * v187 | (v187 >> 31);
  v188 = v3->block;
  v189 = v181 + v188->l[2] + (v183 ^ statem ^ v184) +
         (32 * v186 | ((unsigned __int64)v186 >> 27)) - 899497514;
  v190 = (v183 >> 2) | (v183 << 30);
  v191 = v188->l[3] ^ v188->l[0] ^ v188->l[5] ^ v188->l[11];
  v188->l[3] = 2 * v191 | (v191 >> 31);
  bbx = (unsigned int)v3->block;
  v192 = *(DWORD *)(bbx + 12) + (v190 ^ statem ^ v186) +
         (32 * v189 | ((unsigned __int64)v189 >> 27)) + v184 - 899497514;
  v193 = (v186 >> 2) | (v186 << 30);
  v194 = *(DWORD *)(bbx + 16) ^ *(DWORD *)(bbx + 4) ^ *(DWORD *)(bbx + 24) ^
         *(DWORD *)(bbx + 48);
  *(DWORD *)(bbx + 16) = 2 * v194 | (v194 >> 31);
  v195 = statem + v3->block->l[4] + (v190 ^ v189 ^ v193) +
         (32 * v192 | ((unsigned __int64)v192 >> 27)) - 899497514;
  v196 = (v189 >> 2) | (v189 << 30);
  v197 = v3->block->l[5] ^ v3->block->l[2] ^ v3->block->l[7] ^
         v3->block->l[13];
  v3->block->l[5] = 2 * v197 | (v197 >> 31);
  v198 = v190 + v3->block->l[5] + (v192 ^ v196 ^ v193) +
         (32 * v195 | (v195 >> 27)) - 899497514;
  v199 = (v192 >> 2) | (v192 << 30);
  v200 = v3->block->l[6] ^ v3->block->l[3] ^ v3->block->l[8] ^
         v3->block->l[14];
  v3->block->l[6] = 2 * v200 | (v200 >> 31);
  bby = (unsigned int)v3->block;
  v201 = *(DWORD *)(bby + 24) + (v195 ^ v199 ^ v196) +
         (32 * v198 | ((unsigned __int64)v198 >> 27)) + v193 - 899497514;
  staten = (v195 >> 2) | (v195 << 30);
  v202 = *(DWORD *)(bby + 28) ^ *(DWORD *)(bby + 16) ^ *(DWORD *)(bby + 36) ^
         *(DWORD *)(bby + 60);
  *(DWORD *)(bby + 28) = 2 * v202 | (v202 >> 31);
  v203 = v3->block;
  v204 = v196 + v203->l[7] + (v198 ^ staten ^ v199) +
         (32 * v201 | ((unsigned __int64)v201 >> 27)) - 899497514;
  v205 = (v198 >> 2) | (v198 << 30);
  v206 = v203->l[8] ^ v203->l[0] ^ v203->l[5] ^ v203->l[10];
  v203->l[8] = 2 * v206 | (v206 >> 31);
  bbz = (unsigned int)v3->block;
  v207 = *(DWORD *)(bbz + 32) + (v205 ^ staten ^ v201) +
         (32 * v204 | ((unsigned __int64)v204 >> 27)) + v199 - 899497514;
  v208 = (v201 >> 2) | (v201 << 30);
  v209 = *(DWORD *)(bbz + 36) ^ *(DWORD *)(bbz + 4) ^ *(DWORD *)(bbz + 24) ^
         *(DWORD *)(bbz + 44);
  *(DWORD *)(bbz + 36) = 2 * v209 | (v209 >> 31);
  v210 = staten + v3->block->l[9] + (v205 ^ v204 ^ v208) +
         (32 * v207 | ((unsigned __int64)v207 >> 27)) - 899497514;
  v211 = (v204 >> 2) | (v204 << 30);
  v212 = v3->block->l[10] ^ v3->block->l[2] ^ v3->block->l[7] ^
         v3->block->l[12];
  v3->block->l[10] = 2 * v212 | (v212 >> 31);
  v213 = v205 + v3->block->l[10] + (v207 ^ v211 ^ v208) +
         (32 * v210 | ((unsigned __int64)v210 >> 27)) - 899497514;
  v214 = (v207 >> 2) | (v207 << 30);
  v215 = v3->block->l[11] ^ v3->block->l[3] ^ v3->block->l[8] ^
         v3->block->l[13];
  v3->block->l[11] = 2 * v215 | (v215 >> 31);
  bca = (unsigned int)v3->block;
  v216 = *(DWORD *)(bca + 44) + (v210 ^ v214 ^ v211) +
         (32 * v213 | ((unsigned __int64)v213 >> 27)) + v208 - 899497514;
  stateo = (v210 >> 2) | (v210 << 30);
  v217 = *(DWORD *)(bca + 48) ^ *(DWORD *)(bca + 16) ^ *(DWORD *)(bca + 36) ^
         *(DWORD *)(bca + 56);
  *(DWORD *)(bca + 48) = 2 * v217 | (v217 >> 31);
  v218 = v3->block;
  v219 = v211 + v218->l[12] + (v213 ^ stateo ^ v214) +
         (32 * v216 | ((unsigned __int64)v216 >> 27)) - 899497514;
  v220 = (v213 >> 2) | (v213 << 30);
  v221 = v218->l[13] ^ v218->l[5] ^ v218->l[10] ^ v218->l[15];
  v218->l[13] = 2 * v221 | (v221 >> 31);
  bcb = (unsigned int)v3->block;
  v222 = *(DWORD *)(bcb + 52) + (v220 ^ stateo ^ v216) +
         (32 * v219 | ((unsigned __int64)v219 >> 27)) + v214 - 899497514;
  v223 = (v216 >> 2) | (v216 << 30);
  v224 = *(DWORD *)bcb ^ *(DWORD *)(bcb + 56) ^ *(DWORD *)(bcb + 24) ^
         *(DWORD *)(bcb + 44);
  *(DWORD *)(bcb + 56) = 2 * v224 | (v224 >> 31);
  bcc = (unsigned int)v3->block;
  v225 =
      (unsigned int *)(stateo + *(DWORD *)(bcc + 56) + (v220 ^ v219 ^ v223) +
                       (32 * v222 | ((unsigned __int64)v222 >> 27)) -
                       899497514);
  v226 = (v219 >> 2) | (v219 << 30);
  statep = v225;
  v227 = *(DWORD *)(bcc + 60) ^ *(DWORD *)(bcc + 4) ^ *(DWORD *)(bcc + 28) ^
         *(DWORD *)(bcc + 48);
  *(DWORD *)(bcc + 60) = 2 * v227 | (v227 >> 31);
  v228 = v4[1];
  *v4 +=
      v220 + v3->block->l[15] + (v222 ^ v226 ^ v223) +
      (32 * (DWORD)statep | ((unsigned __int64)(unsigned int)statep >> 27)) -
      899497514;
  v229 = v4[2];
  v4[1] = (unsigned int)statep + v228;
  v230 = v4[4];
  v231 = v4[3];
  v4[2] = ((v222 >> 2) | (v222 << 30)) + v229;
  v4[3] = v226 + v231;
  v4[4] = v223 + v230;

#if 0
  memcpy(Source->block, buffer, sizeof(SHA1_WORKSPACE_BLOCK));
  for (size_t Round = 0; Round < 79; Round++)
  {
	  if (Round )
	  {
		  Transform_0_19(Source, state);
	  }

	  if (Round)
	  {
		  Transform_20_39(Source, state);
	  }

	  if (Round)
	  {
		  Transform_40_59(Source, state);
	  }

	  if (Round)
	  {
		  Transform_60_79(Source, state);
	  }

  }

#endif
}

void MSHA1::Transform_0_19(MSHA1 * Source, unsigned int * state)
{
}

void MSHA1::Transform_20_39(MSHA1 * Source, unsigned int * state)
{
}

void MSHA1::Transform_40_59(MSHA1 * Source, unsigned int * state)
{
}

void MSHA1::Transform_60_79(MSHA1 * Source, unsigned int * state)
{
}
