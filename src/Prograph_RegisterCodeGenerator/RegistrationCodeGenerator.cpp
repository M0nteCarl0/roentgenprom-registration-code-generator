#include "RegistrationCodeGenerator.h"
#include "sha1-master\sha1.h"

RegistrationCodeGenerator::RegistrationCodeGenerator(void)
    {
    }


RegistrationCodeGenerator::~RegistrationCodeGenerator(void)
    {
    }

string RegistrationCodeGenerator::GenerateRegistrationCode(string & IndentficationCode)
{
	SHA1_CTX Sha1;
	char RegistartionCode[255];
	char KeyPhase[255];
	unsigned char Digest[20];
	sprintf(KeyPhase, "ProGraph3-0-0-0%s", IndentficationCode.c_str());
	SHA1Init(&Sha1);
	SHA1Update(&Sha1,(unsigned char*)KeyPhase, strlen(KeyPhase));
	SHA1Final(Digest, &Sha1);
	SHA1FormatDigest(Digest, RegistartionCode, 0);
	return string(RegistartionCode);
}
