#include "CryptoNite.h"
#include <WinCrypt.h>
#include <stdio.h>>
#define SHA1LEN 20

CryptoNite::CryptoNite(void) {}

CryptoNite::~CryptoNite(void) {}

string CryptoNite::GenerateAES1Hash(char *Buffer, size_t &Length,
                                    int &ReportType) {

  FILE* Trace = fopen("TraceSHA1_Cryptonite.log", "w");;
  string Result;
  DWORD dwStatus = 0;
  BOOL bResult = FALSE;
  HCRYPTPROV hProv = 0;
  HCRYPTHASH hHash = 0;
  DWORD cbRead = 0;
  BYTE Hash[SHA1LEN];
  DWORD cbHash = SHA1LEN;
  char Report[255];
  char TempBuffer[5];
  CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
  CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash);
  CryptHashData(hHash, (const BYTE *)Buffer, Length, 0);
  CryptGetHashParam(hHash, HP_HASHVAL, Hash, &cbHash, 0);

  for( int  i = 0;i<20;i++)
      {

      fprintf(Trace, "Original_Digest[%i] = %x\n", i, Hash[i]);
      fflush(Trace);
      }
  fclose(Trace);
#if 0
  int DigestIterator;
  switch (ReportType) {
  case 0: {
    sprintf(TempBuffer, "%02X", Hash[0]);
    BYTE *OneDigest = &Hash[1];
    break;
  }

  case 1: {
    sprintf(TempBuffer, "%u", Hash[0]);
    BYTE *OneDigest = &Hash[1];
    DigestIterator = 19;
    do {
      sprintf(TempBuffer, "%u", (unsigned __int8)*OneDigest);
      strcat(Report, TempBuffer);
      ++OneDigest;
      --DigestIterator;
    } while (DigestIterator);
    Result = string(Report);
    return Result;
    break;
  }

  break;
  }
#endif
  return Result;
}
