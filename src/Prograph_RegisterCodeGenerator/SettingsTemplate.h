#pragma once
#include <Windows.h>
#include <stdint.h>
#include <string>
#include <winreg.h>
using namespace std;

class ProgramSettings {
public:
  ProgramSettings(){};
  ~ProgramSettings(){};
  static string GetRegistryPath(string Result) {
    return Result = string("Software\RoentgenProm\ARCU\Settings");
  }
};

class SettingsTemplate {
public:
  SettingsTemplate(void);
  ~SettingsTemplate(void);

  int OwnWriteProfileIntHKCU(string key, int val);
  int OwnWriteProfileStringHKCU(string key, string val);
  int OwnWriteProfileBinaryHKCU(string key, char *val, DWORD len);

  int OwnGetProfileIntHKCU(string key, int *val);
  int OwnGetProfileStringHKCU(string key, string val);
  int OwnGetProfileBinaryHKCU(string key, char **val, DWORD *len);

  int OwnWriteProfileIntHKLM(string key, int val);
  int OwnWriteProfileStringHKLM(string key, string val);

  int OwnGetProfileBinaryHKLM(string key, char **val, DWORD *len);
  int OwnGetProfileStringHKLM(string key, string val);
  int OwnGetProfileIntHKLM(string key, int *val);
  int CheckNullTerminateAndSet(unsigned int len, const int nMaxRegSZ,
                               string *value);
};
