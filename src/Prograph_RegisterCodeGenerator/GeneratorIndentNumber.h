#pragma once
#include "CPUInformation.h"
#include <Windows.h>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
using namespace std;
class GeneratorIndentNumber {
public:
  GeneratorIndentNumber(void);
  ~GeneratorIndentNumber(void);
  static string GetIndentificationNumber(void);
  static DWORD GetSerialNumberHDD(int Id, string &Result);
  static int GetIndentification(char *DataBufferVideo);
};
