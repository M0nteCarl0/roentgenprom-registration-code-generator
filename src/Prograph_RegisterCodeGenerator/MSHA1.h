#pragma once
#include <Windows.h>
#include <memory>
#include <string>
typedef  union SHA1_WORKSPACE_BLOCK {
  char c[64];
  unsigned int l[16];
}SHA1_WORKSPACE_BLOCK;
using namespace std;
class MSHA1 {
public:
  MSHA1(void);
  static void MSHAInit(MSHA1 *Source);
  ~MSHA1(void);
  static void Reset(MSHA1 *Source);
  static void Final(MSHA1 *Source);
  static void ReportHash(MSHA1 *Source, char *szReport, char uReportType);
  static void Update(MSHA1 *Source, char *data, unsigned int len);
  static void Transform(MSHA1 *Source, unsigned int *state, char *buffer);
  static void Transform_0_19(MSHA1 *Source, unsigned int *state);
  static void Transform_20_39(MSHA1 *Source, unsigned int *state);
  static void Transform_40_59(MSHA1 *Source, unsigned int *state);
  static void Transform_60_79(MSHA1 *Source, unsigned int *state);
  unsigned int state[5];
  unsigned int count[2];
  char buffer[64];
  char workspace[64];
  char digest[20];
  SHA1_WORKSPACE_BLOCK *block;
};
